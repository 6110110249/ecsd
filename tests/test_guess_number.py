import guess.guess_number as guess_number
import unittest
from unittest import mock

class TestGuessNumber(unittest.TestCase):
    
    @mock.patch('random.randint',return_value=int(0))
    def test_guess_int_0_to_0_should_be_0(self, guessValue):
        result = guess_number.guess_int(0,0)
        self.assertEqual(result,0,"should be 0")

    @mock.patch('random.randint',return_value=int(5))
    def test_guess_int_1_to_10_should_be_5(self, guessValue):
        result = guess_number.guess_int(1,10)
        self.assertEqual(result,5,"should be 5")
        
    @mock.patch('random.randint',return_value=int(58))
    def test_guess_int_100_to_10_should_be_58(self, guessValue):
        result = guess_number.guess_int(100,58)
        self.assertEqual(result,58,"should be 58")

    @mock.patch('random.randint',return_value=int(249))
    def test_guess_int_10_to_300_should_be_249(self, guessValue):
        result = guess_number.guess_int(10,300)
        self.assertEqual(result,249,"should be 249")

    @mock.patch('random.randint',return_value=int(63))
    def test_guess_int_minus100_to_200_should_be_63(self, guessValue):
        result = guess_number.guess_int(-100,200)
        self.assertEqual(result,63,"should be 63")

    @mock.patch('random.randint',return_value=int(-312))
    def test_guess_int_minus500_to_minus50_should_be_minus312(self, guessValue):
        result = guess_number.guess_int(-500,-50)
        self.assertEqual(result,-312,"should be -312")

    @mock.patch('random.randint',return_value=float(0.0))
    def test_guess_float_0_to_0_should_be_0point0(self, guessValue):
        result = guess_number.guess_int(0,0)
        self.assertEqual(result,0.0,"should be 0.0")

    @mock.patch('random.uniform',return_value=float(12.7))
    def test_guess_float_1_to_30_should_be_12point7(self, guessValue):
        result = guess_number.guess_float(1,30)
        self.assertEqual(result,12.7,"should be 12.7")
    
    @mock.patch('random.uniform',return_value=float(14.41726))
    def test_guess_float_10point5_to_15point6_should_be_14point41726(self, guessValue):
        result = guess_number.guess_float(10.5,15.6)
        self.assertEqual(result,14.41726,"should be 14.41726")

    @mock.patch('random.uniform',return_value=float(-15.24476))
    def test_guess_float_minus40point81625_to_minus1point7152_should_be_minus15point24476(self, guessValue):
        result = guess_number.guess_float(-40.81625,-1.7152)
        self.assertEqual(result,-15.24476,"should be -15.24476")
        