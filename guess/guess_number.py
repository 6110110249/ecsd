"""A dummy docstring."""
import random


def guess_int(start, stop):
    """
high level support for doing this and that.
"""
    return random.randint(start, stop)


def guess_float(start, stop):
    """
high level support for doing this and that.
"""
    return random.uniform(start, stop)
